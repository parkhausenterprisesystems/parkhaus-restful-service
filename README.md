# API DOCUMENTATION #
API path: https://www.ultimapp.de/tomcat/parkhaus-backend/api/
## GENERAL ##
path: general/

#### create-car ####
* Request
```
{}
```
* Result
```
{
    behinderung: boolean,
    kennzeichen: string,
    color: string,
    wartet: boolean,
    geschlecht: "WEIBLICH"|"MAENNLICH",
    ankunft:number
}
```
#### free-parking-lots ####
* Request
```
{}
```
* Result
```
{
    free:number,
    used:number
}
```
#### get-queue-cars ####
* Request
```
{}
```
* Result
```
[
	{
		behinderung: boolean,
    	kennzeichen: string,
    	color: string,
    	geschlecht: "WEIBLICH"|"MAENNLICH",
    	ankunft:number
	}
]
```

#### get-parkplatz ####
* Request
```
{
	etgae:number,
	parkplatz:number
}
```
* Result
```
{
    etageid:number,
    parkplatzid:number,
    parkplatz:{
   		 id: number,
    	typ: string,
    	belegt: boolean,
    	preis:number,
    	fahrzeug?: {
    		  wartet: boolean,
			  kennzeichen: string,
			  color: string,
			  geschlecht: string,
			  behinderung: boolean,
			  ankunft?: number,
			  ausfahrt?: number
    	}
   }
}
```
#### leave-kfz-from-parking-lot ####
* Request
```
{
	etgae:number,
	parkplatz:number
}
```
* Result
```
{
    foundkfz:boolean,
    fahrzeug:{
    wartet: boolean,
			  kennzeichen: string,
			  color: string,
			  geschlecht: string,
			  behinderung: boolean,
			  ankunft?: number,
			  ausfahrt?: number
    }
}
```
#### reset-parkhaus ####
* Request
```
{
	etagen:number,
	parkinglots:number,
	disabledparking:number,
	womenparking:number,
	price:number
}
```
* Result
```
{}
```
#### get-statistik ####
* Request
```
{
	zeitpunkt?:{
		monat:number,
		jahr:number
	}
}
```
* Result
```
{
	anzahlfahrzeuge:number,
	einnahmen:number,
	ausgaben:number,
	gewinn:number,
	jahresstatistik:[
		{
			anzahlfahrzeuge:number,
	einnahmen:number,
	ausgaben:number,
	gewinn:number,
	monat:number,
	jahr:number,
	current:boolean
		}
	],
	fahrzeuge:[
		{
			etage:number,
			parkplatz:number,
			einfahrt:number,
			ausfahrt:number,
			geparkt:{
				stunden:number,
				minuten:number,
				preis:number
			},
			kennzeichen:string,
			farbe:string,
			geschlecht: "WEIBLICH"|"MAENNLICH",
			behinderung:boolean
		}
	]
}
```
#### leave-random-kfz ####
* Request
```
{}
```
* Result
```
{
	foundkfz:boolean,
	fahrzeug?:{
			  kennzeichen: string,
			  color: string,
			  geschlecht: string,
			  behinderung: boolean,
			  ankunft: number,
			  ausfahrt: number
	}
}
```
#### parkhaus-details ####
* Request
```
{}
```
* Result
```
{
	etagen:[
		{
			id:number,
			normal:{
				frei:number,
				belegt:number
			},
			behinderte:{
				frei:number,
				belegt:number
			},
			frauen:{
				frei:number,
				belegt:number
			},
			parkplaetze:[
				{
					id:number,
					typ:string,
					belegt:boolean,
					fahrzeug?:{
						  kennzeichen: string,
						  color: string,
						  geschlecht: string,
						  behinderung: boolean,
						  ankunft: number,
						  ausfahrt?: number
					}
				}
			]
		}
	]
}
```
