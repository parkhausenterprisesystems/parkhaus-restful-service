package de.parkhaus_enterprise_systems.parkhaus_backend;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import de.parkhaus_enterprise_systems.parkhaus.data.IfEtage;
import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfControllingUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfStatistik;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.Zeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Parkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.IfManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.impl.ManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.util.TimeUtil;
import de.parkhaus_enterprise_systems.parkhaus.util.ZufallsgeneratorUtil;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("general")
public class General
{

	@Path("create-car")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCar(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
		}
		// OUTPUT
		JSONObject output = new JSONObject();
		IfKfz kfz = ZufallsgeneratorUtil.generiereFahrzeug(parkhaus);
		boolean wartet = !parkhaus.fahrzeugEinfahren(kfz);
		if (wartet)
		{
			Resources.QUEUE.add(kfz);
		}
		output.put("color", kfz.getFarbe());
		output.put("kennzeichen", kfz.getKennzeichen());
		output.put("geschlecht", kfz.getGeschlecht().name());
		output.put("behinderung", kfz.hatBehinderung());
		output.put("wartet", wartet);
		output.put("ankunft", kfz.getEinfahrtszeit());
		output.put("ausfahrt", kfz.getAusfahrtszeit());
		return Response.status(200).entity(output.toString()).build();
	}

	@Path("get-queue-cars")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getQueueCars(String inp)
	{
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
		}
		// OUTPUT
		JSONArray output = new JSONArray();
		for (IfKfz z : Resources.QUEUE)
		{
			output.put(Util.getFahrzeugInformation(z));
		}
		return Response.status(200).entity(output.toString()).build();
	}

	@Path("free-parking-lots")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getFreeParkingLots(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
		}
		// OUTPUT
		JSONObject output = new JSONObject();
		int free = parkhaus.getFreieParkplaetze(EnParkplatz.BEHINDERT)
				+ parkhaus.getFreieParkplaetze(EnParkplatz.FRAUEN) + parkhaus.getFreieParkplaetze(EnParkplatz.NORMAL);
		int belegt = parkhaus.getBelegteParkplaetze(EnParkplatz.BEHINDERT)
				+ parkhaus.getBelegteParkplaetze(EnParkplatz.FRAUEN)
				+ parkhaus.getBelegteParkplaetze(EnParkplatz.NORMAL);
		output.put("free", free);
		output.put("used", belegt);
		return Response.status(200).entity(output.toString()).build();
	}

	@Path("parkhaus-details")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getParkhausDetails(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
		}

		// OUTPUT
		JSONObject output = new JSONObject();

		JSONArray arr = new JSONArray();
		int index = 0;
		for (IfEtage etage : parkhaus.getEtagen())
		{
			JSONObject et = new JSONObject();
			et.put("id", index);
			et.put("normal", Util.getParkplatzEtagenInformation(etage, EnParkplatz.NORMAL));
			et.put("behinderte", Util.getParkplatzEtagenInformation(etage, EnParkplatz.BEHINDERT));
			et.put("frauen", Util.getParkplatzEtagenInformation(etage, EnParkplatz.FRAUEN));
			JSONArray plz = new JSONArray();
			int pID = 0;
			for (IfParkplatz p : etage.getAlleParkplaetze())
			{
				JSONObject parkplatzInformation = Util.getParkplatzInformation(p);
				parkplatzInformation.put("id", pID);
				plz.put(parkplatzInformation);
				pID++;
			}
			et.put("parkplaetze", plz);
			arr.put(et);
			index++;
		}
		output.put("etagen", arr);

		return Response.status(200).entity(output.toString()).build();
	}

	@Path("get-etage")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getEtage(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
			int etageId = input.getInt("etage");
			List<IfEtage> etagen = parkhaus.getEtagen();
			// OUTPUT
			JSONObject etage = new JSONObject();
			if (etageId >= 0 && etageId < etagen.size())
			{
				IfEtage e = etagen.get(etageId);
				etage.put("id", etageId);
				etage.put("normal", Util.getParkplatzEtagenInformation(e, EnParkplatz.NORMAL));
				etage.put("behinderte", Util.getParkplatzEtagenInformation(e, EnParkplatz.BEHINDERT));
				etage.put("frauen", Util.getParkplatzEtagenInformation(e, EnParkplatz.FRAUEN));
				JSONArray plz = new JSONArray();
				int pID = 0;
				for (IfParkplatz p : e.getAlleParkplaetze())
				{
					JSONObject parkplatzInformation = Util.getParkplatzInformation(p);
					parkplatzInformation.put("id", pID);
					plz.put(parkplatzInformation);
					pID++;
				}
				etage.put("parkplaetze", plz);
			}
			return Response.status(200).entity(etage.toString()).build();
		}
		return Response.status(500).build();
	}

	@Path("get-parkplatz")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getParkplatz(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
			int etageId = input.getInt("etage");
			int parkplatzId = input.getInt("parkplatz");
			List<IfEtage> etagen = parkhaus.getEtagen();
			// OUTPUT
			JSONObject parkplatz = new JSONObject();
			if (etageId >= 0 && etageId < etagen.size())
			{
				IfEtage e = etagen.get(etageId);
				if (parkplatzId >= 0 && parkplatzId < e.getAlleParkplaetze().size())
				{
					parkplatz.put("etageid", etageId);
					parkplatz.put("parkplatzid", parkplatzId);
					IfParkplatz p = e.getAlleParkplaetze().get(parkplatzId);
					parkplatz.put("parkplatz", Util.getParkplatzInformation(p));
					parkplatz.put("preis", parkhaus.getManagementUnit().getPreisProStunde());
					return Response.status(200).entity(parkplatz.toString()).build();
				}
			}
		}
		return Response.status(500).build();
	}

	@Path("leave-random-kfz")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response leaveRandomKfz(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
		}
		// OUTPUT
		JSONObject output = new JSONObject();
		IfKfz firstKfz = parkhaus.getFirstKfz();
		if (firstKfz != null)
		{
			boolean fahrzeugAusfahren = parkhaus.fahrzeugAusfahren(firstKfz);
			JSONObject fahrzeugInformation = Util.getFahrzeugInformation(firstKfz);
			fahrzeugInformation.put("einfahrt", firstKfz.getEinfahrtszeit());
			fahrzeugInformation.put("ausfahrt", firstKfz.getAusfahrtszeit());
			output.put("fahrzeug", fahrzeugInformation);
			output.put("foundkfz", true);
		}
		else
		{
			output.put("foundkfz", false);
		}
		return Response.status(200).entity(output.toString()).build();
	}

	@Path("leave-kfz-from-parking-lot")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response leaveKfzFromParkingLot(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
			int etageId = input.getInt("etage");
			int parkplatzId = input.getInt("parkplatz");
			List<IfEtage> etagen = parkhaus.getEtagen();
			// OUTPUT
			JSONObject output = new JSONObject();
			if (etageId >= 0 && etageId < etagen.size())
			{
				IfEtage e = etagen.get(etageId);
				if (parkplatzId >= 0 && parkplatzId < e.getAlleParkplaetze().size())
				{
					IfParkplatz p = e.getAlleParkplaetze().get(parkplatzId);
					// HIER AUSFAHREN
					IfKfz firstKfz = p.getGeparktesFahrzeug();
					if (firstKfz != null)
					{
						boolean fahrzeugAusfahren = parkhaus.fahrzeugAusfahren(firstKfz);
						JSONObject fahrzeugInformation = Util.getFahrzeugInformation(firstKfz);
						fahrzeugInformation.put("ankunft", firstKfz.getEinfahrtszeit());
						fahrzeugInformation.put("ausfahrt", firstKfz.getAusfahrtszeit());
						output.put("fahrzeug", fahrzeugInformation);
						output.put("foundkfz", true);
						// TODO QUEUE CHECKEN
					}
					else
					{
						output.put("foundkfz", false);
					}
					return Response.status(200).entity(output.toString()).build();
				}
			}
		}
		return Response.status(500).build();
	}

	@Path("reset-parkhaus")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response resetParkhaus(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		// INPUT
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			JSONObject input = new JSONObject(inp);
			int etagen = input.getInt("etagen");
			int parkinglots = input.getInt("parkinglots");
			int disabledparking = input.getInt("disabledparking");
			int womenparking = input.getInt("womenparking");
			double price = input.getDouble("price");
			if (etagen > 0 && parkinglots > 0 && disabledparking > 0 && price >= 0.1)
			{
				IfManagementUnit mangement = new ManagementUnit(price);
				IfParkhaus neuesParkhaus = new Parkhaus(etagen, parkinglots, disabledparking, womenparking, mangement);
				Resources.PARKHAUS = neuesParkhaus;
				Resources.QUEUE.clear();
				JSONObject output = new JSONObject();
				return Response.status(200).entity(output.toString()).build();
			}
		}
		return Response.status(500).build();
	}

	@Path("get-statistik")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getStatistik(String inp)
	{
		IfParkhaus parkhaus = Resources.PARKHAUS;
		if (inp != null && inp.contains("{") && inp.contains("}"))
		{
			// INPUT
			JSONObject input = new JSONObject(inp);
			IfControllingUnit controlling = parkhaus.getControllingUnit();
			// VON
			IfZeitpunkt von = new Zeitpunkt();
			if (input.has("zeitpunkt") && input.getJSONObject("zeitpunkt") != null)
			{
				JSONObject vonObj = input.getJSONObject("zeitpunkt");
				if (vonObj.has("monat") && vonObj.has("jahr"))
				{
					von = new Zeitpunkt(vonObj.getInt("monat"), vonObj.getInt("jahr"));
				}
			}

			JSONObject output = Util.getZeitpunktDetails(von, controlling);
			// JAHRESSTAT.
			JSONArray jahres = new JSONArray();

			for (IfZeitpunkt z : getHalbesJahrDavor(von))
			{
				JSONObject p = Util.getZeitpunktDetails(z, controlling);
				p.put("monat", z.getMonat());
				p.put("jahr", z.getJahr());
				p.put("current", false);
				jahres.put(p);
			}
			JSONObject p = Util.getZeitpunktDetails(von, controlling);
			p.put("monat", von.getMonat());
			p.put("jahr", von.getJahr());
			p.put("current", true);
			jahres.put(p);
			for (IfZeitpunkt z : getHalbesJahrDanach(von))
			{
				JSONObject p1 = Util.getZeitpunktDetails(z, controlling);
				p1.put("monat", z.getMonat());
				p1.put("jahr", z.getJahr());
				p1.put("current", false);
				jahres.put(p1);
			}
			output.put("jahresstatistik", jahres);

			JSONArray fahrzeuge = new JSONArray();
			for (IfStatistik stat : controlling.getStatstikForZeitpunkt(von))
			{
				JSONObject fahrzeug = new JSONObject();
				fahrzeug.put("etage", stat.getEtageIndex() + 1);
				fahrzeug.put("parkplatz", stat.getParkplatzIndex() + 1);
				fahrzeug.put("einfahrt", stat.getEinfahrtszeit());
				fahrzeug.put("ausfahrt", stat.getAusfahrtszeit());
				JSONObject geparkt = new JSONObject();
				geparkt.put("stunden", TimeUtil.getGeparkteStunden(stat.getEinfahrtszeit(), stat.getAusfahrtszeit()));
				geparkt.put("minuten", TimeUtil.getGeparkteMinuten(stat.getEinfahrtszeit(), stat.getAusfahrtszeit()));

				geparkt.put("preis", stat.getBezahlterPreis());
				fahrzeug.put("geparkt", geparkt);
				fahrzeug.put("farbe", stat.getFahrzeugFarbe());
				fahrzeug.put("kennzeichen", stat.getKennzeichen());
				fahrzeug.put("geschlecht", stat.getFahrzeugGeschlecht());
				fahrzeug.put("behinderung", stat.hasFahrzeugbehinderung());
				fahrzeuge.put(fahrzeug);
			}
			output.put("fahrzeuge", fahrzeuge);

			return Response.status(200).entity(output.toString()).build();
		}
		return Response.status(500).build();
	}

	private List<IfZeitpunkt> getHalbesJahrDavor(IfZeitpunkt zeitpunkt)
	{
		List<IfZeitpunkt> z = new ArrayList<IfZeitpunkt>();
		int monat = zeitpunkt.getMonat();
		int jahr = zeitpunkt.getJahr();
		for (int i = 0; i < 6; i++)
		{
			monat -= 1;
			if (monat == 0)
			{
				monat = 12;
				jahr -= 1;
			}
			IfZeitpunkt zn = new Zeitpunkt(monat, jahr);
			z.add(zn);
		}
		return z;
	}

	private List<IfZeitpunkt> getHalbesJahrDanach(IfZeitpunkt zeitpunkt)
	{
		List<IfZeitpunkt> z = new ArrayList<IfZeitpunkt>();
		int monat = zeitpunkt.getMonat();
		int jahr = zeitpunkt.getJahr();
		for (int i = 0; i < 6; i++)
		{
			monat += 1;
			if (monat > 12)
			{
				monat = 1;
				jahr += 1;
			}
			IfZeitpunkt zn = new Zeitpunkt(monat, jahr);
			z.add(zn);
		}
		return z;
	}
}
