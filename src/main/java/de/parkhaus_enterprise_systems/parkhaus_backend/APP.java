/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus_backend;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Parkhaus;

/**
 * @author Kevin
 *
 */
@ApplicationPath("/")
public class APP extends Application
{
	public APP()
	{
		if (Resources.PARKHAUS == null)
		{
			Resources.PARKHAUS = new Parkhaus(5, 20);
		}
		if (Resources.QUEUE == null)
		{
			Resources.QUEUE = new ArrayList<IfKfz>();
		}
		System.out.println("Parkhaus API STARTED at " + (new Date()).toString() + " ...");
	}

}
