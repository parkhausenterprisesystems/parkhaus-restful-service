/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus_backend;

import org.json.JSONObject;

import de.parkhaus_enterprise_systems.parkhaus.data.IfEtage;
import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfControllingUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;

/**
 * @author Kevin
 *
 */
public class Util
{

	public static JSONObject getParkplatzEtagenInformation(IfEtage etage, EnParkplatz park)
	{
		JSONObject p = new JSONObject();
		p.put("frei", etage.getFreieParkplaetze(park));
		p.put("belegt", etage.getBelegteParkplaetze(park));
		return p;
	}

	public static JSONObject getParkplatzInformation(IfParkplatz p)
	{
		JSONObject o = new JSONObject();
		o.put("typ", p.getTyp().name());
		o.put("belegt", p.istBelegt());
		o.put("fahrzeug", getFahrzeugInformation(p.getGeparktesFahrzeug()));
		return o;
	}

	public static JSONObject getFahrzeugInformation(IfKfz kfz)
	{
		if (kfz == null)
		{
			return null;
		}
		JSONObject output = new JSONObject();
		output.put("color", kfz.getFarbe());
		output.put("kennzeichen", kfz.getKennzeichen());
		output.put("geschlecht", kfz.getGeschlecht().name());
		output.put("behinderung", kfz.hatBehinderung());
		output.put("ankunft", kfz.getEinfahrtszeit());
		output.put("ausfahrt", kfz.getAusfahrtszeit());
		return output;
	}

	public static JSONObject getZeitpunktDetails(IfZeitpunkt von, IfControllingUnit controlling)
	{
		JSONObject output = new JSONObject();
		output.put("anzahlfahrzeuge", controlling.getAnzahlFahrzeuge(von));
		output.put("einnhamen", controlling.getEinnahmen(von));
		output.put("ausgaben", controlling.getAusgaben(von));
		output.put("gewinn", controlling.getGewinn(von));
		return output;
	}
}
