/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus_backend;

import java.util.List;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;

/**
 * @author Kevin
 *
 */
public class Resources
{
	// PARKHAUS INSTANCE
	public static IfParkhaus PARKHAUS = null;
	public static List<IfKfz> QUEUE = null;
}
